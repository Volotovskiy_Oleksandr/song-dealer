package roman.pidkostelny.dealer.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor

@Entity
public class Bands {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String photo;

    @Column(unique = true)
    private String name;

    private String genreName;

    private String alive;

    private String url;


    @ManyToMany(mappedBy = "bands")
    private List<Songs> songs = new ArrayList<>();

    @ManyToOne
    private Person person;


}
