package roman.pidkostelny.dealer.exception;

public class WrongInp extends Exception {

    public WrongInp() {
    }

    public WrongInp(String message) {
        super(message);
    }
}
