package roman.pidkostelny.dealer.dto.respons;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import roman.pidkostelny.dealer.entity.Bands;
import roman.pidkostelny.dealer.entity.Songs;

@Getter
@Setter
@NoArgsConstructor
public class BandsResponse {

    private Long id;

    private String name;

    private String genreName;

    private String alive;

    private String url;

    private String photo;


    public BandsResponse(Bands bands) {
        id = bands.getId();
        name = bands.getName();
        genreName = bands.getGenreName();
        alive = bands.getAlive();
        url = bands.getUrl();
        photo = bands.getPhoto();
    }
}
